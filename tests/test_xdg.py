import os
import collections
import pytest

from mgcomm.env import home
from mgcomm.xdg import basedir


eu = os.path.expanduser
opj = os.path.join
sep = os.path.sep


@pytest.fixture
def appcfg(tmpdir, monkeypatch):
    app = 'appcfg_app'
    file = 'appcfg_file'

    root = tmpdir.mkdir('local')
    cache = root.mkdir('cache')
    config = root.mkdir('config')
    data = root.mkdir('data')
    runtime = root.mkdir('runtime')

    cache.mkdir(app).join(file).write('')
    config.mkdir(app).join(file).write('')
    data.mkdir(app).join(file).write('')
    runtime.mkdir(app).join(file).write('')

    Cfg = collections.namedtuple(
        'AppCfg', ['app', 'file', 'cache', 'config', 'data', 'runtime'])

    return Cfg(app, file, str(cache), str(config), str(data), str(runtime))


@pytest.fixture
def clear_xdg_vars(monkeypatch):
    monkeypatch.delenv('XDG_CACHE_HOME', raising=False)
    monkeypatch.delenv('XDG_CONFIG_HOME', raising=False)
    monkeypatch.delenv('XDG_CONFIG_DIRS', raising=False)
    monkeypatch.delenv('XDG_DATA_HOME', raising=False)
    monkeypatch.delenv('XDG_DATA_DIRS', raising=False)
    monkeypatch.delenv('XDG_RUNTIME_DIR', raising=False)


def test_basedir_path_not_found(clear_xdg_vars):
    '''Tests behaviour of basedir() when it fails to find a path according to
    XDG spec'''
    assert basedir('cache') == opj(home(), '.cache')
    assert basedir('config') == opj(home(), '.config')
    assert basedir('data') == opj(home(), '.local', 'share')
    assert basedir('runtime') == opj(sep, 'run', 'user', str(os.getuid()))


@pytest.mark.parametrize('what', ['cache', 'runtime'])
def test_basedir_cache_and_runtime(what, clear_xdg_vars, appcfg, monkeypatch):
    '''Test behaviour of basedir('cache') and basedir('runtime')'''
    realdir = getattr(appcfg, what)

    if what == 'runtime':
        monkeypatch.setenv('XDG_%s_DIR' % what.upper(), realdir)
    else:
        monkeypatch.setenv('XDG_%s_HOME' % what.upper(), realdir)
    assert basedir(what) == realdir
    assert basedir(what, file='foo') == opj(realdir, 'foo')
    assert basedir(what, subdir='bar') == opj(realdir, 'bar')
    assert basedir(what, subdir='bar', file='foo') == opj(realdir, 'bar', 'foo')


@pytest.mark.parametrize('what', ['config', 'data'])
def test_basedir_config_and_data(what, clear_xdg_vars, appcfg, monkeypatch):
    '''Test behaviour of basedir('config') and basedir('data') - they have two
    environment variables to search.'''
    realdir = getattr(appcfg, what)
    badhome = opj(sep, 'sth')

    monkeypatch.setenv('XDG_%s_HOME' % what.upper(), realdir)

    # Only _HOME
    assert basedir(what) == realdir
    assert basedir(what, file='foo') == opj(realdir, 'foo')
    assert basedir(what, subdir='bar') == opj(realdir, 'bar')
    assert basedir(what, subdir='bar', file='foo') == opj(realdir, 'bar', 'foo')

    # Bot _HOME and _DIRS; searched file is found in _DIRS
    monkeypatch.setenv('XDG_%s_HOME' % what.upper(), badhome)
    monkeypatch.setenv('XDG_%s_DIRS' % what.upper(), realdir)

    assert basedir(what) == badhome
    assert basedir(what, subdir='foo', file='bar') == opj(badhome, 'foo', 'bar')
    found = basedir(what, subdir=appcfg.app, file=appcfg.file)
    assert found == opj(realdir, appcfg.app, appcfg.file)
    assert os.path.exists(found)

    # Bot _HOME and _DIRS; searched file is found in _HOME
    monkeypatch.setenv('XDG_%s_HOME' % what.upper(), realdir)
    monkeypatch.setenv('XDG_%s_DIRS' % what.upper(), badhome)

    assert basedir(what) == realdir
    assert basedir(what, subdir='foo', file='bar') == opj(realdir, 'foo', 'bar')

    found = basedir(what, subdir=appcfg.app, file=appcfg.file)
    assert found == opj(realdir, appcfg.app, appcfg.file)
    assert os.path.exists(found)

    # Only _DIRS
    monkeypatch.delenv('XDG_%s_HOME' % what.upper())
    monkeypatch.setenv('XDG_%s_DIRS' % what.upper(), realdir)
    if what == 'config':
        assert basedir(what) == opj(home(), '.config')
    else:
        assert basedir(what) == opj(home(), '.local', 'share')

    found = basedir(what, subdir=appcfg.app, file=appcfg.file)
    assert found == opj(realdir, appcfg.app, appcfg.file)
    assert os.path.exists(found)


def test_basedir_incorrect_base():
    with pytest.raises(ValueError):
        basedir('foo')
