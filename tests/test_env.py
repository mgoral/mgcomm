import os
import pytest

from mgcomm.env import home, var_split


def test_home():
    assert home() is not None
    assert home() == os.path.expanduser('~')


def test_var_split(monkeypatch):
    monkeypatch.setenv('my_var', 'abc:def:ghi')
    monkeypatch.delenv('MY_VAR', raising=False)
    assert var_split('my_var') == ['abc', 'def', 'ghi']
    assert var_split('MY_VAR') == []
    assert var_split('MY_VAR', default='sth') == ['sth']
    assert var_split('MY_VAR', default=['sth']) == ['sth']
    assert var_split('MY_VAR', default=['sth', 'else']) == ['sth', 'else']

    monkeypatch.delenv('my_var', raising=False)
    assert var_split('my_var') == []

    monkeypatch.setenv('second_var', 'abc,def, ghi')
    assert var_split('second_var') == ['abc,def, ghi']
    assert var_split('second_var', sep=',') == ['abc', 'def', ' ghi']

    monkeypatch.delenv('not_existing', raising=False)
    assert var_split('not_existing', '123', sep=',') == ['123']
    assert var_split('not_existing', '123,456', sep=',') == ['123', '456']
    assert var_split('not_existing', '123,456') == ['123,456']
    assert var_split('not_existing', '123:456') == ['123', '456']
    assert var_split('not_existing', ' 123 : 456 ') == [' 123 ', ' 456 ']
