mgcomm
======

Usually when writing some CLI apps I end up writing the same stuff over and over
again. Like searching for configuration files according to Freedesktop's
standards. Or handling user input from either which can be collected from stdin,
read from a file or piped in. This kind of stuff. So I decided to collect it in
a small (probably personal) library which actually has some tests, sane
interface and most important: can be easily used just by adding it to
requirements.txt.
