# mgcomm 0.2.0 changelog

## Changes since 0.1.0:

* Replaced xdg.config and xdg.config_dir with general purpose xdg.basedir()
